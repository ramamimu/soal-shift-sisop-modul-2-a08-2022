#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <stdio.h>

int main() {
    pid_t child_id;
    int status;

    // Membuat directori shift/drakor
    child_id = fork();
    if (child_id < 0) 
        exit(EXIT_FAILURE);
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", "shift2/drakor", NULL};
        execv("/bin/mkdir", argv);
    }
    while (wait(NULL) != child_id);
    printf("\n--PROSES MEMBUAT DIRECTORY BERHASIL\n\n");

    // Change directory
    if ((chdir("/home/ariefbadrus/shift2/drakor/")) < 0) {
        exit(EXIT_FAILURE);
    }

    // Download file drakor.zip
    pid_t child1 = fork();
    if (child1< 0) 
        exit(EXIT_FAILURE);
    if (child1 == 0) {
        // Mempersiapkan file drakor.zip di gdrive
        // Link donwload : https://drive.google.com/uc?export=download&id=1YUrUOJThMQsvB5rXKQj43LyAtlGEMuF3
        char *argv[] = {"wget", "--no-check-certificate", "https://drive.google.com/uc?export=download&id=1YUrUOJThMQsvB5rXKQj43LyAtlGEMuF3", "-O", "drakor.zip", NULL};
        execv("/bin/wget", argv);
    } 
    while (wait(NULL) != child1);
    printf("--PROSES DOWNLOAD BERHASIL\n\n");

    // Unzip drakor.zip
    pid_t child2 = fork();
    if (child2< 0) 
        exit(EXIT_FAILURE);
    if (child2 == 0) {
        // sleep(10);
        char *argv[] = {"unzip", "drakor.zip", NULL};
        execv("/bin/unzip", argv);
    } 
    while (wait(NULL) != child2);
    printf("\n--PROSES UNZIP BERHASIL\n\n");

    // Remove file serta folder tidak penting
    pid_t child3 = fork();
    if (child3< 0) 
        exit(EXIT_FAILURE);
    if (child3 == 0) {
        // sleep(20);
        char *argv[] = {"rm", "-r", "coding", "song", "trash", "drakor.zip", NULL};
        execv("/bin/rm", argv);
    } else {
        printf("--PROSES REMOVE UNIMPORTANT FILE BERHASIL\n\n");
        sleep(5);

        // Pengolahan File
        DIR *folder;
        struct dirent *ep;
        int count = 0;

        folder = opendir("/home/ariefbadrus/drakor");

        if (folder != NULL)
        {
            // Mengolah satu persatu file
            while ((ep = readdir (folder)))
            {
                printf("--mengolah poster \n");
                if(strcmp(ep->d_name, ".") !=0 && strcmp(ep->d_name, "..") !=0)
                {
                    char namaPoster[1001];
                    strcpy(namaPoster, ep->d_name);

                    // Menghilangkan ekstensi .jpg
                    char *delimiter = strtok(namaPoster, ".");
                    strcpy(namaPoster, delimiter);
                    
                    char tahun[101];
                    char tahunTmp[101];
                    char judul[101];
                    char judulTmp[101];
                    char kategori[101];
                    char kategoriTmp[101];
                    int flag=0;

                    // Menelusuri nama file dipisahkan tanda ';' menggunakan strtok
                    delimiter = strtok(namaPoster, "_;");
                    int token = 0;
                    while(delimiter != NULL) {
                        if (token==0 || token==3) {
                            strcpy(judul, delimiter);
                            if(token == 0) {
                                strcpy(judulTmp, delimiter);
                            }
                        }
                        if (token==1 || token==4){
                            strcpy(tahun, delimiter);
                            if(token == 1){
                                strcpy(tahunTmp, delimiter);
                            }
                        }
                        if (token==2 || token==5) {
                            strcpy(kategori, delimiter);
                            if(token == 2){
                                strcpy(kategoriTmp, delimiter);
                            }
                            
                            child_id = fork();
                            if (child_id < 0) {
                                exit(EXIT_FAILURE);
                            }                       
                            if (child_id == 0) {
                                // Membuat folder
                                char *argv[] = {"mkdir", "-p", kategori, NULL};
                                execv("/bin/mkdir", argv);
                            } else {
                                sleep(1);

                                FILE * fp;
                                FILE * file;
                                char path[101];

                                strcpy(path, "/home/ariefbadrus/shift2/drakor/");
                                strcat(path, kategori);
                                strcat(path, "/data.txt");

                                if (file = fopen(path, "r+")){
                                    fclose(file);
                                } else {
                                    fp = fopen(path, "w+");
                                    fprintf(fp, "kategori : %s\n\n", kategori);
                                    fclose(fp);
                                }
                            }
                            while (wait(NULL) != child_id);
                        } 
                        if (token == 3)
                            flag = 1;
                        token++;
                        delimiter = strtok(NULL, "_;");
                    }
                    
                    // Copy poster ke folder sesuai kategori
                    char judulBaru[101];
                    strcpy(judulBaru, "/home/ariefbadrus/shift2/drakor/");
                    strcat(judulBaru, kategori);
                    strcat(judulBaru, "/");
                    strcat(judulBaru, judul);
                    strcat(judulBaru, ".png");
            
                    char source[101];
                    strcpy(source, "/home/ariefbadrus/shift2/drakor/");
                    strcat(source, ep->d_name);

                    pid_t child4;
                    child4 = fork();
                    if (child4< 0) 
                        exit(EXIT_FAILURE);
                    if (child4 == 0) {
                        char *argv[] = {"cp", "-r", source, judulBaru, NULL};
                        execv("/bin/cp", argv);
                    } else {
                        sleep(1);
                        
                        FILE * fp;
                        char path[101];

                        strcpy(path, "/home/ariefbadrus/shift2/drakor/");
                        strcat(path, kategori);
                        strcat(path, "/data.txt");
                        
                        fp = fopen(path, "a+");
                        fprintf(fp, "nama : %s\nrilis : tahun %s\n\n", judul, tahun);
                        fclose(fp);
                    }
                    while (wait(NULL) != child4);

                    // Proses copy yang sama untuk satu file 2 poster
                    if (flag == 1){
                        char judulBaru[101];
                        strcpy(judulBaru, "/home/ariefbadrus/shift2/drakor/");
                        strcat(judulBaru, kategoriTmp);
                        strcat(judulBaru, "/");
                        strcat(judulBaru, judulTmp);
                        strcat(judulBaru, ".png");
                
                        char source[101];
                        strcpy(source, "/home/ariefbadrus/shift2/drakor/");
                        strcat(source, ep->d_name);

                        pid_t child5;
                        child5 = fork();
                        if (child5< 0) 
                            exit(EXIT_FAILURE);
                        if (child5 == 0) {
                            char *argv[] = {"cp", "-r", source, judulBaru, NULL};
                            execv("/bin/cp", argv);
                        } else {
                            sleep(1);
                            
                            FILE * fp;
                            char path[101];

                            strcpy(path, "/home/ariefbadrus/shift2/drakor/");
                            strcat(path, kategori);
                            strcat(path, "/data.txt");
                            
                            fp = fopen(path, "a+");
                            fprintf(fp, "nama : %s\nrilis : tahun %s\n\n", judulTmp, tahunTmp);
                            fclose(fp);
                        }
                        while (wait(NULL) != child5);
                    }

                    // Menghapus file lama
                    pid_t child6;
                    child6 = fork();
                    if (child6< 0) 
                        exit(EXIT_FAILURE);
                    if (child6 == 0) {
                        char * argv[] = {"rm", "-r", ep->d_name, NULL};
                        execv("/bin/rm", argv);
                        printf("PROSES hapus file lama BERHASIL\n");
                    }
                    while (wait(NULL) != child6);

                    if(count==15){
                        (void) closedir (folder);
                        printf("\n--PROSES MENGOLAH POSTER BERHASIL\n");
                    }
                    count++;

                }
            }
        }
    }
    while (wait(NULL) != child3);

    printf("PROSES BERHASIL\n");

    return 0;
    //--------------------------------------------------------------------------

}