# Soal 1

> Memanggil script menggunakan exec() dan fork()

```c
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main() {
  pid_t child_id;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"mkdir", "/home/ram/sisop/M2/data/gacha_gacha", NULL};
    execv("/bin/mkdir", argv);
  } else {
    // this is parent
    char *argv[] = {"bash", "/home/ram/sisop/M2/script/soal1/soal1.sh", NULL};
    execv("/bin/bash", argv);
  }
}
```

## A

> Mendownload file characters dan file weapons dari link dan mengekstrak kedua file tersebut

```bash
# Database item characters :
# https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view
echo `wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp' -O ~/sisop/M2/data/characters.zip`
echo `unzip ~/sisop/M2/data/characters -d ~/sisop/M2/data/`
echo `rm ~/sisop/M2/data/characters.zip`

# Database item weapons :
# https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view
echo `wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT' -O ~/sisop/M2/data/weapons.zip`
echo `unzip ~/sisop/M2/data/weapons -d ~/sisop/M2/data/`
echo `rm ~/sisop/M2/data/weapons.zip`
```

> Dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory

```c
  if (child_id == 0) {
    // this is child
    char *argv[] = {"mkdir", "/home/ram/sisop/M2/data/gacha_gacha", NULL};
    execv("/bin/mkdir", argv);
  } else {
    // this is parent
    char *argv[] = {"bash", "/home/ram/sisop/M2/script/soal1/soal1.sh", NULL};
    execv("/bin/bash", argv);
  }
```

## B

> Setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters.

```bash
randOddEven=$[$RANDOM % 2 + 1]
if [ $randOddEven == 1 ]
then
    determine="characters"
    echo "ganjil"
    randChar=$[$RANDOM % $countCharFile + 1]
    while isExistChar $randChar;
    do
        echo "ada double ganjil $randChar"
        randChar=$[$RANDOM % $countCharFile]
    done
    arrChar+=($randChar)
    echo "ARRAY char ${arrChar[@]}"
    fileName=$(getNameFile $randChar ~/sisop/M2/data/characters)
    getName=`cat ~/sisop/M2/data/characters/$fileName | awk 'NR==2{print}'`
    getRarity=`cat ~/sisop/M2/data/characters/$fileName | awk 'NR==6{print}'`
else
    determine="weapons"
    echo "genap"
    randWeap=$[$RANDOM % $countWeapFile + 1]
    while isExistWeap $randWeap;
    do
        echo "ada double genap $randWeap"
        randWeap=$[$RANDOM % $countWeapFile]
    done
    arrWeap+=($randWeap)
    echo "ARRAY weap ${arrWeap[@]}"
    fileName=$(getNameFile $randWeap ~/sisop/M2/data/weapons)
    getName=`cat ~/sisop/M2/data/weapons/$fileName | awk 'NR==2{print}'`
    getRarity=`cat ~/sisop/M2/data/weapons/$fileName | awk 'NR==5{print}'`
fi

```

> Setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut dan Setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha.

```bash
if [ $(($count%90)) -eq 0 ]
then
    folderContain=$(($count+90))
    currentFolder="total_gacha_"$folderContain
    echo `mkdir ~/sisop/M2/data/gacha_gacha/$currentFolder`
fi

if [ $(($count%10)) -eq 0 ]
then
    fileContain=$(($count+10))
    currentFile=$time"_gacha_"$fileContain
    echo `touch ~/sisop/M2/data/gacha_gacha/$currentFolder/$currentFile.txt`
    arrChar=()
    arrWeap=()
fi
```

> hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan BERBEDA

```bash
isExistChar(){
    for i in "${arrChar[@]}"
    do
        if [ "$i" -eq "$1" ]
        then
            return 0
        fi
    done
    return 1
}
isExistWeap(){
    for i in "${arrWeap[@]}"
    do
        if [ "$i" -eq "$1" ]
        then
            return 0
        fi
    done
    return 1
}
...
if [ $randOddEven == 1 ]
then
    randChar=$[$RANDOM % $countCharFile + 1]
    while isExistChar $randChar;
    do
        randChar=$[$RANDOM % $countCharFile]
    done
    arrChar+=($randChar)
    ...
else
    while isExistWeap $randWeap;
    do
        randWeap=$[$RANDOM % $countWeapFile]
    done
    arrWeap+=($randWeap)
    ...
fi
```

Setiap file yang akan ditulis pada file .txt yang sama akan dimasukkan ke dalam sebuah array. array tersebut difungsikan untuk mengecek apakah judul dari weapon dan character sudah dipakai di file yang sama melalui fungsi `isExistChar()` dan `isExistWeap()`.

## C

> Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}\_gacha\_{jumlah-gacha} misal 04:44:12_gacha_120.txt

```bash
currentFile=$time"_gacha_"$fileContain
format=$count"_"$determine"_"$getRarity"_"$getName"_"$primogems
...
echo $format >> ~/sisop/M2/data/gacha_gacha/$currentFolder/$currentFile.txt

```

> format penamaan untuk setiap folder nya adalah total_gacha\_{jumlah-gacha}, misal total_gacha_270.

```bash
folderContain=$(($count+90))
currentFolder="total_gacha_"$folderContain
echo `mkdir ~/sisop/M2/data/gacha_gacha/$currentFolder`
...
echo $format >> ~/sisop/M2/data/gacha_gacha/$currentFolder/$currentFile.txt

```

> setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.

```bash
while [ $SECONDS -lt $end ]
do
    ...
    format=$count"_"$determine"_"$getRarity"_"$getName"_"$primogems
    echo $format >> ~/sisop/M2/data/gacha_gacha/$currentFolder/$currentFile.txt

    sleep 1s
done
```

## D

> Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}\_[tipe-item]\_{rarity}\_{name}\_{sisa-primogems}. Contoh : 157_characters_5_Albedo_53880

```bash
primogems=$((79000-$count*160))
format=$count"_"$determine"_"$getRarity"_"$getName"_"$primogems
echo $format >> ~/sisop/M2/data/gacha_gacha/$currentFolder/$currentFile.txt
```

## E

> Proses untuk melakukan gacha item akan dimulai pada 30 Maret jam 04:44.

```bash
timeStart=`date +%D`
dateStart=`date +%T`

if [[ $timeStart == "03/30/22" && $dateStart == 04:44:* ]]
then

```

> 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)

```bash
if [[ $timeStart == "03/30/22" && $dateStart == "07:44:00" ]]
then

```

## Hasil

![permission.txt](foto/1.1.png)
![permission.txt](foto/1.2.png)
![permission.txt](foto/1.3.png)
![permission.txt](foto/1.4.png)

## Kesulitan

Tidak ada

# Soal 2: Mengelola Foto Poster Serial Drama Korea

## 2.1 Mengextract zip dan menghapus folder tidak penting

Pertama yang harus dilakukan adalah membuat direktori `/home/[user]/shift2/drakor` karena berdasarkan note dan ketentuan soal, folder `shift2` dan `drakor` dibuatkan oleh program. Hal ini dapat dilakukan dengan membuat proses menggunakan `fork()` dan `execv` dengan argumen `mkdir`.

```c
    // Membuat directori shift/drakor
    child_id = fork();
    if (child_id < 0)
        exit(EXIT_FAILURE);
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", "shift2/drakor", NULL};
        execv("/bin/mkdir", argv);
    }
    while (wait(NULL) != child_id);
    printf("\n--PROSES MEMBUAT DIRECTORY BERHASIL\n");
```

Kemudian kita mengubah directory pekerjaan kita dengan menggunakan `chdir`. Hal ini dilakukan karena untuk langkah-langkah selanjutnya akan dilakuan di dalam folder drakor sehingga akan mempermudah pekerjaan.

```c
    // Change directory
    if ((chdir("/home/ariefbadrus/shift2/drakor/")) < 0) {
        exit(EXIT_FAILURE);
    }
```

Selanjutnya kita mendapatkan file `drakor.zip` dengan mendownload file di gdrive. Untuk dapat mendownload file, dapat melakukan pembuatan proses dengan menggunakan `fork()` dan `execv` dengan argumen `wget`.

```c
    // Download file drakor.zip
    pid_t child1 = fork();
    if (child1< 0)
        exit(EXIT_FAILURE);
    if (child1 == 0) {
        // Mempersiapkan file drakor.zip di gdrive
        // Link donwload : https://drive.google.com/uc?export=download&id=1YUrUOJThMQsvB5rXKQj43LyAtlGEMuF3
        char *argv[] = {"wget", "--no-check-certificate", "https://drive.google.com/uc?export=download&id=1YUrUOJThMQsvB5rXKQj43LyAtlGEMuF3", "-O", "drakor.zip", NULL};
        execv("/bin/wget", argv);
        
    }
    while (wait(NULL) != child1);
    printf("--PROSES DOWNLOAD BERHASIL\n");
```

Langkah selanjutnya adalah meng-unzip file `drakor.zip` yang telah didownload agar dapat melakukan proses pengelolaan foto nanti. Untuk dapat melakukan task tersebut dapat melakukan pembuatan proses dengan menggunakan `fork()` dan `execv` dengan argumen `unzip`.

```c
    // Unzip drakor.zip
    pid_t child2 = fork();
    if (child2< 0)
        exit(EXIT_FAILURE);
    if (child2 == 0) {
        char *argv[] = {"unzip", "drakor.zip", NULL};
        execv("/bin/unzip", argv);
    }
    while (wait(NULL) != child2);
    printf("\n--PROSES UNZIP BERHASIL\n");
```

Kemudian tugas selanjutnya sesuai soal adalah menghapus folder tidak penting dan file `drakor.zip` itu sendiri. Berdasarkan observasi di dalam isi file zip, adapun folder folder yang tidak penting adalah sebagai berikut:

- `coding`
- `song`
- `trash`

Untuk dapat menghapus folder tersebut dapat melakukan pembuatan proses dengan menggunakan `fork()` dan `execv` dengan argumen `rm`.

```c
    // Remove file serta folder tidak penting
    pid_t child3 = fork();
    if (child3< 0)
        exit(EXIT_FAILURE);
    if (child3 == 0) {
        char *argv[] = {"rm", "-r", "coding", "song", "trash", "drakor.zip", NULL};
        execv("/bin/rm", argv);
    } else {
        ...
    }
    while (wait(NULL) != child3);
```

## 2.2 Membuat folder sesuai kategori poster

Petama untuk dapat melakukan pengelolaan poster, kami menggunakan `DIR` dan `dirent` yang terdapat dalam library `dirent.h` untuk dapat melakukan penelusuran terhadap file file yang terdapat dalam suatu directory dalam hal ini adalah `\home\ariefbadrus\shift2\drakor`.

```c
        DIR *folder;
        struct dirent *ep;

        folder = opendir("/home/ariefbadrus/drakor");

        if (folder != NULL) {
            ...
        }
```

Dilakukan penelusuran untuk setiap file di dalam directory menggunakan `readdir()`.

```c
            // Mengolah satu persatu file
            while ((ep = readdir (folder))) {
                printf("--mengolah poster \n");
                if(strcmp(ep->d_name, ".") !=0 && strcmp(ep->d_name, "..") !=0) {
                    ...
                }
                ...
            }
```

Karena informasi untuk nama, kategori, dan tahun poster terdapat dalam nama file masing masing poster yang dipisahkan oleh tanda `';'`, maka kami menggunakan fungsi `strtok()` yang terdapat didalam library `string.h`. Dengan menggunakan fungsi tersebut, kami dapat mendapatkan masing masing kata yang dipisah dengan suatu delimiter.

Pertama kami mendapatkan nama file poster yang terdapat dalam `ep->d_name`.

```c
                    char namaPoster[1001];
                    strcpy(namaPoster, ep->d_name);
```

Kemudian kami menghapus ekstensi file `.png` agar mempermudah di task selanjutnya menggunkan fungsi `strtok()` dengan delimiter `'.'`.

```c
                    // Menghilangkan ekstensi .png
                    char *delimiter = strtok(namaPoster, ".");
                    strcpy(namaPoster, delimiter);
```

Variabel-variabel diinisialisasikan untuk menyimpan informasi seperti kategori, tahun, dan nama poster.

```c
                    char tahun[101];
                    char tahunTmp[101];
                    char judul[101];
                    char judulTmp[101];
                    char kategori[101];
                    char kategoriTmp[101];
                    int flag=0;
```

Variabel `flag` berfungsi sebagai penanda nantinya untuk file poster yang didalamnya berisi 2 poster. File yang berisi 2 poster dipisahkan oleh delimiter `'_'`.

Kemudian langkah selanjutnya dilakukan penelusuran selama kata yang dipisahkan oleh delimiter `';'` dan `'_'` masih ada.

```c
                    // Menelusuri nama file dipisahkan tanda ';' menggunakan strtok
                    delimiter = strtok(namaPoster, "_;");
                    int token = 0;
                    while(delimiter != NULL) {
                        if (token==0 || token==3) {
                            strcpy(judul, delimiter);
                            if(token == 0) {
                                strcpy(judulTmp, delimiter);
                            }
                        }
                        if (token==1 || token==4){
                            strcpy(tahun, delimiter);
                            if(token == 1){
                                strcpy(tahunTmp, delimiter);
                            }
                        }
                        if (token==2 || token==5) {
                            strcpy(kategori, delimiter);
                            if(token == 2){
                                strcpy(kategoriTmp, delimiter);
                            }
                            
                            child_id = fork();
                            if (child_id < 0) {
                                exit(EXIT_FAILURE);
                            }                       
                            if (child_id == 0) {
                                // Membuat folder
                                char *argv[] = {"mkdir", "-p", kategori, NULL};
                                execv("/bin/mkdir", argv);
                            } else {
                                sleep(1);

                                FILE * fp;
                                FILE * file;
                                char path[101];

                                strcpy(path, "/home/ariefbadrus/shift2/drakor/");
                                strcat(path, kategori);
                                strcat(path, "/data.txt");

                                if (file = fopen(path, "r+")){
                                    fclose(file);
                                } else {
                                    fp = fopen(path, "w+");
                                    fprintf(fp, "kategori : %s\n\n", kategori);
                                    fclose(fp);
                                }
                            }
                            while (wait(NULL) != child_id);
                        } 
                        if (token == 3)
                            flag = 1;
                        token++;
                        delimiter = strtok(NULL, "_;");
                    }
```

Penjelasan mengenai token digambarkan melalui gambar dibawah ini.
![DeskripsiNamaPoster](foto/2.1_DeskripsiNamaPoster.png)
Sehingga:

- Untuk token 0 atau 3, judul poster disimpan dalam variabel `judul`
- Untuk token 1 atau 4, tahun poster disimpan dalam variabel `tahun`
- Untuk token 2 atau 5, kategori poster disimpan dalam variabel `kategori`.

Menyimpan masing masing informasi dapat menggunakan fungsi `strcpy`.

Kembali ke tugas soal 2.2 dimana kita diminta untuk membuat folder di masing-masing kategori. Tugas ini dapat kita lakukan ketika token sama dengan 2 atau 5. Untuk dapat membuat folder dapat melakukan pembuatan proses dengan menggunakan `fork()` dan `execv()` dengan argumen `mkdir`.

```c
                            child_id = fork();
                            if (child_id < 0) {
                                exit(EXIT_FAILURE);
                            }
                            if (child_id == 0) {
                                // Membuat folder
                                char *argv[] = {"mkdir", "-p", kategori, NULL};
                                execv("/bin/mkdir", argv);
                            }
                            while (wait(NULL) != child_id);
```

## 2.3 Memindahkan poster ke folder dengan kategori yang sesuai

Langkah pertama yang harus dilakukan adalah menyimpan directory dan judul baru sesuai format soal. Dimana formatnya adalah `judul.png`.

```c
                    // Copy poster ke folder sesuai kategori
                    char judulBaru[101];
                    strcpy(judulBaru, "/home/ariefbadrus/shift2/drakor/");
                    strcat(judulBaru, kategori);
                    strcat(judulBaru, "/");
                    strcat(judulBaru, judul);
                    strcat(judulBaru, ".png");
```

Kemudian menyimpan directory dan judul poster asli tanpa dirubah.

```c
                    char source[101];
                    strcpy(source, "/home/ariefbadrus/shift2/drakor/");
                    strcat(source, ep->d_name);
```

Selanjutnya proses pemindahan file. Proses pemindahan file tidak dilakukan dengan `move/cut` melainkan dengan `copy` hal ini dikarenakan satu file poster bisa terdiri dari 2 poster berbeda, sehingga kaitannya dengan soal 2.4 nanti. Untuk dapat meng-copy file poster sesuai kategori dapat melakukan pembuatan proses menggunakan `fork()` dan `execv()` dengan argumen `cp`.

```c
                    pid_t child4;
                    child4 = fork();
                    if (child4< 0)
                        exit(EXIT_FAILURE);
                    if (child4 == 0) {
                        char *argv[] = {"cp", source, judulBaru, NULL};
                        execv("/bin/cp", argv);
                    } else {
                        ...
                    }
                    while (wait(NULL) != child4);
```

## 2.4 Foto yang lebih dari satu poster dipindah ke masing-masing folder sesuai kategori

Pertama kita simpan masing masing kategori pada variabel `kategoriTmp` dan judul pada variabel `judulTmp` ketika token sama dengan 0 dan 2.

```c
                            if(token == 0) {
                                strcpy(judulTmp, delimiter);
                            }
```

```c
                            if(token == 2){
                                strcpy(kategoriTmp, delimiter);
                            }
```

Kemudian kita set `flag=1` ketika token mencapai 3 (artinya dalam satu foto terdapat 2 poster).

```c
                        if (token == 3)
                            flag = 1;
```

Sehingga proses pemindahan foto dilakukan ketika `flag=1`. Proses pemindahan foto sama persis dengan soal 2.3 mulai dari menyimpan judul baru, source, dan proses copy.

```c
                    // Proses copy yang sama untuk satu file 2 poster
                    if (flag == 1){
                        char judulBaru[101];
                        strcpy(judulBaru, "/home/ariefbadrus/shift2/drakor/");
                        strcat(judulBaru, kategoriTmp);
                        strcat(judulBaru, "/");
                        strcat(judulBaru, judulTmp);
                        strcat(judulBaru, ".png");

                        char source[101];
                        strcpy(source, "/home/ariefbadrus/shift2/drakor/");
                        strcat(source, ep->d_name);

                        pid_t child5;
                        child5 = fork();
                        if (child5< 0)
                            exit(EXIT_FAILURE);
                        if (child5 == 0) {
                            char *argv[] = {"cp", source, judulBaru, NULL};
                            execv("/bin/cp", argv);
                        } else {

                        }
                        while (wait(NULL) != child5);
                    }
```

Tidak lupa juga di akhir proses, file asli foto atau poster pada folder `drakor` dihapus karena semua file sudah dicopy ke folder sesuai kategori masing-masing. Untuk dapat menghapus file dapat melakukan pembuatan proses menggunakan `fork()` dan `execv()` dengan argumen `rm`.

```c
                    // Menghapus file lama
                    pid_t child6;
                    child6 = fork();
                    if (child6< 0)
                        exit(EXIT_FAILURE);
                    if (child6 == 0) {
                        char * argv[] = {"rm", "-r", ep->d_name, NULL};
                        execv("/bin/rm", argv);
                        printf("PROSES hapus file lama BERHASIL\n");
                    }
                    while (wait(NULL) != child6);
```

## 2.5 Membuat file `data.txt` di setiap folder kategori yang berisi nama dan tahun rilis poster

Pertama kita perlu membuat file `data.txt` berisi nama kategori dengan format seperti berikut.

```
kategori : action
```

Selanjutnya mendapatkan format path sesuai kategori agar mempermudah pekerjaan. Kemudian dilakukan pengecekan apakah file  `data.txt` sudah terbuat di folder tersebut. Apabila tidak maka file dibuat.

``` c
                                strcpy(path, "/home/ariefbadrus/shift2/drakor/");
                                strcat(path, kategori);
                                strcat(path, "/data.txt");

                                if (file = fopen(path, "r+")){
                                    fclose(file);
                                } else {
                                    fp = fopen(path, "w+");
                                    fprintf(fp, "kategori : %s\n\n", kategori);
                                    fclose(fp);
                                }
```

Kemudian untuk masing masing poster ketika melakukan proses pemindahan ke folder kategori masing masing. Juga dilakukan pencatatan informasi mengenai poster sesuai format berikut.

```
nama : descendant-of-the-sun
rilis : tahun 2016
```

Perintah yang dilakukan adalah sebagai berikut.

``` c
                        } else {
                            sleep(1);
                            
                            FILE * fp;
                            char path[101];

                            strcpy(path, "/home/ariefbadrus/shift2/drakor/");
                            strcat(path, kategori);
                            strcat(path, "/data.txt");
                            
                            fp = fopen(path, "a+");
                            fprintf(fp, "nama : %s\nrilis : tahun %s\n\n", judulTmp, tahunTmp);
                            fclose(fp);
                        }
```

Proses yang sama juga dilakukan untuk kondisi `flag=1` ketika dalam satu foto poster terdapat dua poster.

Di akhir program tidak lupa dilakukan perintah `closedir()` supaya perulangan berhenti. Karena folder `drakor` tidak akan kosong disebabkan terdapat folder folder kategori.

``` c
                    if(count==15){
                        (void) closedir (folder);
                        printf("\n--PROSES MENGOLAH POSTER BERHASIL\n");
                    }
                    count++;
```

## 2.6 Hasil

- Run Program dengan syntax `gcc soal2.c -o soal2 && ./soal2`
  ![RunProgram](foto/2.2_RunProgram.png)
- Isi folder `drakor`
  ![FolderDrakor](foto/2.3_FolderDrakor.png)
- Isi folder `action` dan isi file `data.txt`
  ![FolderAction](foto/2.4_FolderAction.png)

## 2.7 Kesulitan

Adapun kesulitan yang saya alami selama mengerjakan soal 2 ini antara lain:

- Kurang paham process sehingga terkadang ada process yang berjalan dan ada yang tidak
- Bingung cara sorting list poster berdasarkan tahun dan dituliskan di file data.txt menggunakan bahasa c sehingga harus revisi

# Soal 3

## 3.1 Membuat directory DARAT dan AIR

Pada file `soal3.c` terdapat dua fungsi, yaitu pembuatan direktori `/home/ram/modul2`dan pemanggilan file bash `soal3.sh` yang mana semua process ada pada file bash tersebut. Pada tahap pertama dilakukan fork untuk membuat directory darat dan air.

```c
int main() {
  pid_t child_id;
  int status;
  child_id = fork();

  if (child_id < 0) {
    // Jika gagal membuat proses baru, program akan berhenti
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"mkdir", "/home/ram/modul2", NULL};
    execv("/bin/mkdir", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
    char *argv[] = {"bash", "/home/ram/sisop/M2/script/soal3/soal3.sh", NULL};
    execv("/bin/bash", argv);
  }
}
```

Memasuki ke bash file `soal3.sh`, Pembuatan directory darat dan air dilakukan selang 3 detik. Jadi pertama akan dibuat directory darat dan 3 detik setelahnya dibuat untuk directory air. Digunakan `sleep 3s` untuk mengatur durasi selang pembuatan kedua directory tersebut.

```bash
    sleep 3s
    echo `mkdir ~/modul2/darat`
    sleep 3s
    echo `mkdir ~/modul2/air`
```

## 3.2 Unzip dan Sorting

Di tahap ini kita akan mengunzip file `animal.zip` dan menyortir hewan darat dan air ke dalam folder directory masing-masing.

```
    echo `unzip ~/Downloads/animal.zip -d ~/modul2`
    echo `mv ~/modul2/animal/**air**.jpg ~/modul2/air`
    echo `mv ~/modul2/animal/**darat**.jpg ~/modul2/darat`
```

Setelah semua file darat dan air dipindahkan ke folder masing-masing, Kita akan mendelete folder animal yang berisikan hewan yang tidak memiliki keterangan yang diminta dengan command `rm`

```bash
echo `rm -r ~/modul2/animal`
```

Tahap selanjutnya kita akan mendelete file-file burung pada folder darat dengan command yang sama.

```bash
echo `rm -r ~/modul2/darat/**bird**.jpg`
```

## 3.3 Membuat file `list.txt` di dalam folder air

Terakhir, Untuk membuat file `list.txt` di dalam folder air kita akan membuat file tersebut di luar folder terlebih dahulu karena jumlah file akan berubah jika dibuat langsung pada folder air. Setelah itu untuk setiap file

```bash
echo `touch ~/modul2/list.txt`
```

Untuk melakukan pemberian permission tiap file dilakukan sebuah perulangan sebagai berikut.

```bash
for((i=2; i<=$total; i=i+1))
    do
        strTitle="ls -l ~/modul2/air | awk '{print \$9}' | awk 'NR == $i {print}'"
        strUser="ls -l ~/modul2/air | awk '{print \$3}' | awk 'NR == $i {print}'"
        strPermission="ls -l ~/modul2/air | awk 'NR == $i {print}'"
        printPerm=`eval $strPermission`
        printUser=`eval $strUser`
        printTitle=`eval $strTitle`
        echo ${printPerm:1:3}"_"$printUser"_"$printTitle >> ~/modul2/list.txt
    done
```

Setelah itu, Kita akan memindahkan file `list.txt` ke dalam folder air.

```bash
echo `mv ~/modul2/list.txt ~/modul2/air/.`
fi
```

## 3.4 Hasil

![permission.txt](foto/3.1.png)
![permission.txt](foto/3.2.png)
![permission.txt](foto/3.3.png)
![permission.txt](foto/3.4.png)

## 3.5 Kesulitan

Tidak ada
