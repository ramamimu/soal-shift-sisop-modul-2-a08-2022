#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>


int main() {
  pid_t child_id;
  int status;
  child_id = fork();

  if (child_id < 0) {
    // Jika gagal membuat proses baru, program akan berhenti    
    exit(EXIT_FAILURE); 
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"mkdir", "/home/ram/modul2", NULL};
    execv("/bin/mkdir", argv);
  } else {
    // this is parent
    while ((wait(&status)) > 0);
    char *argv[] = {"bash", "/home/ram/sisop/M2/script/soal3/soal3.sh", NULL};
    execv("/bin/bash", argv);
  }
}