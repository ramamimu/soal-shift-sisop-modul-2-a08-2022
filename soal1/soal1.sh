#!/bin/bash 

# environment demo yang perlu disiapkan
# uncomment if then waktunya atau set di current time. INGAT ada 2
# end time dipercepat saat demo (kalau dianjurkan)


# Database item characters :
# https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view
echo `wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp' -O ~/sisop/M2/data/characters.zip`
echo `unzip ~/sisop/M2/data/characters -d ~/sisop/M2/data/`
echo `rm ~/sisop/M2/data/characters.zip`

# Database item weapons :
# https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view
echo `wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT' -O ~/sisop/M2/data/weapons.zip`
echo `unzip ~/sisop/M2/data/weapons -d ~/sisop/M2/data/`
echo `rm ~/sisop/M2/data/weapons.zip`

# date + %D
# 03/19/22
# date +%T
# 13:53:03
timeStart=`date +%D` 
dateStart=`date +%T` 

if [[ $timeStart == "03/30/22" && $dateStart == 04:44:* ]]
then
    if [ ! -d ~/sisop/M2/data/gacha_gacha ]
    then
        echo `mkdir ~/sisop/M2/data/gacha_gacha`
    fi

    # 0 is true, 1 false
    isExistChar(){
        for i in "${arrChar[@]}"
        do
            if [ "$i" -eq "$1" ]
            then
                return 0
            fi
        done
        return 1
    }
    isExistWeap(){
        for i in "${arrWeap[@]}"
        do
            if [ "$i" -eq "$1" ]
            then
                return 0
            fi
        done
        return 1
    }
    isNotExistChar(){
        for i in "${arrChar[@]}"
        do
            if [ "$i" -eq "$1" ]
            then
                return 1
            fi
        done
        return 0
    }
    isNotExistWeap(){
        for i in "${arrWeap[@]}"
        do
            if [ "$i" -eq "$1" ]
            then
                return 1
            fi
        done
        return 0
    }


    # $1 = order, $2 = path
    getNameFile(){
        # echo "In function $1"
        # echo "In function $2"
        # echo "In function arr ${arrChar[@]}" 
        # echo "In function arr ${arrWeap[@]}" 
        # temp=
        command="ls $2 | awk 'NR == $1{print}'"
        test=`eval $command`
        echo "$test"
    }

    arrChar=()
    arrWeap=()
    # | grep -F -x -z $1
    # | grep -F -x -z $2

    # selama 
    # 493 didapat dari 79000/160 (jumlah gem dan cost tiap gacha)
    end=$((SECONDS+493))
    # end=$((SECONDS+60))
    count=0
    countCharFile=`ls ~/sisop/M2/data/characters | wc -l`
    countWeapFile=`ls ~/sisop/M2/data/weapons | wc -l`
    echo "total file-file $countCharFile $countWeapFile"
    while [ $SECONDS -lt $end ]
    do
        # manajemen direktori
        time=`date +%T`
        # buat folder
        if [ $(($count%90)) -eq 0 ]
        then
            folderContain=$(($count+90))
            currentFolder="total_gacha_"$folderContain
            echo `mkdir ~/sisop/M2/data/gacha_gacha/$currentFolder`
        fi
        # buat file
        if [ $(($count%10)) -eq 0 ]
        then
            fileContain=$(($count+10))
            currentFile=$time"_gacha_"$fileContain
            echo `touch ~/sisop/M2/data/gacha_gacha/$currentFolder/$currentFile.txt`
            arrChar=()
            arrWeap=()
        fi
        # random 1 dan 2, probability ganjil:genap = 50:50
        randOddEven=$[$RANDOM % 2 + 1]

        # name
        # awk 'NR==2{print}' aether.json
        # rarity
        # awk 'NR==6{print}' aether.json
        if [ $randOddEven == 1 ]
        then
            determine="characters"
            echo "ganjil"
            randChar=$[$RANDOM % $countCharFile + 1]
            while isExistChar $randChar;
            do
                echo "ada double ganjil $randChar"
                randChar=$[$RANDOM % $countCharFile]
            done
            arrChar+=($randChar)
            echo "ARRAY char ${arrChar[@]}"
            fileName=$(getNameFile $randChar ~/sisop/M2/data/characters)
            getName=`cat ~/sisop/M2/data/characters/$fileName | awk 'NR==2{print}'`
            getRarity=`cat ~/sisop/M2/data/characters/$fileName | awk 'NR==6{print}'`
        else
            determine="weapons"
            echo "genap"
            randWeap=$[$RANDOM % $countWeapFile + 1]
            while isExistWeap $randWeap;
            do
                echo "ada double genap $randWeap"
                randWeap=$[$RANDOM % $countWeapFile]
            done
            arrWeap+=($randWeap)
            echo "ARRAY weap ${arrWeap[@]}"
            fileName=$(getNameFile $randWeap ~/sisop/M2/data/weapons)
            getName=`cat ~/sisop/M2/data/weapons/$fileName | awk 'NR==2{print}'`
            getRarity=`cat ~/sisop/M2/data/weapons/$fileName | awk 'NR==5{print}'`
        fi
        # awk -F '"' ' {print $4}' percobaan.txt 
        getName=`echo $getName | awk -F '"' ' {print $4}'` 
        getRarity=`echo $getRarity | awk -F '"' ' {print $4}'`
        # echo $getName >> ~/sisop/M2/data/gacha_gacha/$currentFolder/$currentFile.txt
        # echo $getRarity >> ~/sisop/M2/data/gacha_gacha/$currentFolder/$currentFile.txt
        # format kurang di sini
        echo "ini hasilnya "$fileName
        echo "RANDOM odd even $randOddEven"
        echo "RANDOM char $randChar"
        echo "RANDOM weapon $randWeapon"
        echo $count 
        count=$(($count+1))

        # {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}
        # 157_characters_5_Albedo_53880
        primogems=$((79000-$count*160))
        format=$count"_"$determine"_"$getRarity"_"$getName"_"$primogems
        echo $format >> ~/sisop/M2/data/gacha_gacha/$currentFolder/$currentFile.txt
            
        sleep 1s
    done
fi

if [[ $timeStart == "03/30/22" && $dateStart == "07:44:00" ]]
then
    echo `zip --password "satuduatiga" ~/sisop/M2/data/not_safe_for_wibu.zip ~/sisop/M2/data/gacha_gacha`
    echo `rm -r ~/sisop/M2/data/gacha_gacha`    
fi