#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main() {
  pid_t child_id;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    char *argv[] = {"mkdir", "/home/ram/sisop/M2/data/gacha_gacha", NULL};
    execv("/bin/mkdir", argv);
  } else {
    // this is parent
    char *argv[] = {"bash", "/home/ram/sisop/M2/script/soal1/soal1.sh", NULL};
    execv("/bin/bash", argv);
  }
}